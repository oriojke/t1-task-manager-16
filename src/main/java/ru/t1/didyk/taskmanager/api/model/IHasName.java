package ru.t1.didyk.taskmanager.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
