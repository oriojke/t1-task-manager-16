package ru.t1.didyk.taskmanager.exception.field;

public final class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Error! Id is empty.");
    }

}
